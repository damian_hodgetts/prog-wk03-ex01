﻿using System;

namespace Week03_Exercise01
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var colours = new string[5] { "red", "blue", "orange", "white", "black" };

			for (var i = 0; i < colours.Length; i++)
			{
				Console.WriteLine(colours[i]);
			}
		}
	}
}
